#!/bin/bash

conda install python=3.8.3
conda install requests=2.23.0
conda install -c conda-forge tqdm
conda install beautifulsoup4=4.9.0
conda install pandas=1.0.3
conda install -c anaconda pymongo