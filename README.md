# SINTA-JMI

----
## Summary

This repo is a collection of source codes and datasets to reproduce the sinta-jmi paper. The codes include the web crawler to generate the dataset, the visualization of the dataset, and the simulation codes.

----
## Installation

I'm using Conda.

1. Install [Conda](https://docs.conda.io/en/latest/miniconda.html).
2. Run the following.

```sh

    conda create -n sinta-jmi

    conda activate sinta-jmi

    yes | bash ./installpackageconda.sh

    conda activate sinta-jmi
    
```


---

## Table of Folders 


- [web_crawl](https://git.ecdf.ed.ac.uk/jmi/sinta-jmi/tree/master/web_crawl): a collection of codes to scrape the SINTA website
- [dataset](https://git.ecdf.ed.ac.uk/jmi/sinta-jmi/tree/master/dataset): final datasets
- [visualization](https://git.ecdf.ed.ac.uk/jmi/sinta-jmi/tree/master/visualization): a collection of codes to visualize the datasets
- [website](https://git.ecdf.ed.ac.uk/jmi/sinta-jmi/tree/master/website): for now, mongodb is used to do the quick search for SJR values


----
## Contributors

* **Ardimas Purwita** - *Initial work* - ardimasandipurwita@outlook.com


## License

This project is licensed under the MIT License

