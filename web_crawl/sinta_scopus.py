"""
    ex: python sinta_scopus.py --tablename sinta_name_id_09062020232959 --min-idx 1 --max-idx 20
"""


import sys
import os
import string

import requests
from bs4 import BeautifulSoup
import re
from tqdm import tqdm

import logging
from datetime import datetime

from collections.abc import Iterable
import itertools
import csv

import pandas as pd
import json

"""
This initial version is written by Ardimas
"""
__version__ = 0.1 

__min_idx__ = 1

__timeout__ = 20 # in s

__usage__ = """Usage: sinta_scopus [options] ...
Scraping publications of authors listed in Scopus from SINTA website 
Options:
  --tablename                 pick the table name in './dataset/', ex: sinta_name_id_09062020232959
  --min-idx                     set the minimum index, default = 1
  --max-idx                     set the maximum index, default depends on the tablename
  -h, --help                    show this help message
  -v, --version                 show version of this command
"""

# http://sinta.ristekbrin.go.id/authors/detail?page=2&id=3731&view=documentsscopus
__authors_pub_page_prefix__ = 'http://sinta.ristekbrin.go.id/authors/detail?page='
__authors_pub_page_infix__ = '&id='
__authors_pub_page_postfix__ = '&view=documentsscopus'


# create a log file
now = datetime.now()
dt_string = now.strftime("%d%m%Y%H%M%S")

# check wheter the log folder exists
# if not create one
if not os.path.exists(os.getcwd() + '/log'):
    os.makedirs(os.getcwd() + '/log')

logging.basicConfig(filename='./log/sinta_scopus_'+dt_string+'.log',level=logging.DEBUG)

def flatten(arr):
    """ Flatten a messy iterable object into 
    an 1D iterable object.
    Examples
    --------
    >>> x = [[1,2],[1],[1,2,3,[4,5]]]
    >>> y = list(flatten(x))
    [1,2,1,1,2,3,4,5]
    
    """
    for i in arr:
        if isinstance(i,Iterable) and not isinstance(i,str):
            yield from flatten(i)
        else:
            yield i

class ArgumentError(Exception):
    pass

def console():
    try:
        outputParsing = parse_args()

        table = pd.read_csv(os.getcwd() +'/dataset/' + outputParsing['tablename'] + '.csv')
        num_rows = len(table.index)

        data_to_be_dump = {} # this is a container for the JSON objects

        for idy in tqdm(range(outputParsing['minIdx'],outputParsing['maxIdx']+1)):
        
            auth_id = table['sinta_id'][idy]
            auth_name = table['name'][idy]

            # get the url for author's pub list
            url_page = __authors_pub_page_prefix__ + str(1) + \
                __authors_pub_page_infix__ + str(auth_id) + __authors_pub_page_postfix__

            # try:
            #     page = requests.get(url_page, timeout=__timeout__)
            # except requests.exceptions.Timeout as err:
            #     # try one more time 
            #     page = requests.get(url_page, timeout=__timeout__)
            page = requests.get(url_page, timeout=__timeout__)
            while not page.ok:
                page = requests.get(url_page, timeout=__timeout__)

            soup = BeautifulSoup(page.content, 'html.parser')

            # get the number of pages
            tables = soup.find_all(class_='uk-width-large-1-2 table-footer')
            tmplist = re.findall(r'\d*\d',tables[0].contents[0]) 
            num_pages = int(tmplist[1])

            data_to_be_dump[str(auth_id)] = []
            data_to_be_dump[str(auth_id)].append({'name': str(auth_name)})

            logging.debug('Author ID: ' + str(auth_id) + ', author name: ' + auth_name)
            logging.debug('There are ' + str(num_pages) + ' pages')

            for idx in tqdm(range(1,num_pages+1)):

                # get the url for author's pub list
                url_page = __authors_pub_page_prefix__ + str(idx) + \
                    __authors_pub_page_infix__ + str(auth_id) + __authors_pub_page_postfix__

                try:
                    page = requests.get(url_page, timeout=__timeout__)
                except requests.exceptions.Timeout as err:
                    # try one more time 
                    page = requests.get(url_page, timeout=__timeout__)

                soup = BeautifulSoup(page.content, 'html.parser')

                # get the list of citation numbers from the idx-th page
                tables = soup.find_all(class_='index-val uk-text-center')
                num_pubs_at_idx_page = len(tables)
                quartile = [tables[tmpidx].contents[0] for tmpidx in range(0,num_pubs_at_idx_page,2)]
                citation_num = [tables[tmpidx].contents[0] for tmpidx in range(1,num_pubs_at_idx_page,2)]

                # get the list of publications from the idx-th page
                tables = soup.find_all(class_='uk-description-list-line')

                for idz in range(int(num_pubs_at_idx_page/2)):
                    
                    try:
                        pub_title = tables[idz].contents[1].contents[1].contents[0]
                    except:
                        pub_title = ''

                    try:
                        pub_journal = tables[idz].contents[3].contents[0].lstrip().rstrip() # lstrip and rstrip to remove the leading and ending spaces
                    except:
                        pub_journal = ''

                    data_to_be_dump[str(auth_id)].append(
                        {
                            'title': str(pub_title),
                            'pub_name': str(pub_journal),
                            'citation_num': str(citation_num[idz]),
                            'quartile': str(quartile[idz])
                        }
                        )

            with open(os.getcwd() + '/dataset/sinta_scopus_'+dt_string+'.json', 'w') as jsonfile:
                json.dump(data_to_be_dump, jsonfile)




    except(ArgumentError):
        sys.stderr.write(__usage__)

def parse_args():

    args = sys.argv[1:]

    minIdx = __min_idx__;

    # check wheter the dataset folder exists
    # if not create one
    if not os.path.exists(os.getcwd() + '/dataset'):
        os.makedirs(os.getcwd() + '/dataset')
        logging.warning("Create a folder named '%s' ", 'dataset')

    i = 0
    while i < len(args):
        op = args[i]
        if op in ("-h","--help"):
            raise ArgumentError
        if op in ("-v","--version"):
            print('Version: '+ str(__version__))
        elif op in ("--tablename"):
            tablename = args[i+1]
            i += 1
        elif op in ("--min-idx"):
            minIdx = int(args[i+1])
            i += 1
        elif op in ("--max-idx"):
            maxIdx = int(args[i+1])
            i += 1
        i += 1

    try:
        table = pd.read_csv(os.getcwd() +'/dataset/' + tablename + '.csv')
    except:
        errorMsg = os.getcwd() +'/dataset/' + tablename + '.csv' + ' does not exist!!'
        print(errorMsg)
        logging.error(errorMsg)
        raise ArgumentError

    if 'maxIdx' not in locals():
        maxIdx = len(table.index)

    if (minIdx < 1) | (maxIdx > len(table.index)):
        logging.error('page number must be between ' + str(__min_idx__) + ' and ' + str(len(table.index)))
        raise ArgumentError

    logging.info('index number:' + str(minIdx) + '-' + str(maxIdx))
    logging.info('tablename: ' + os.getcwd() +'/dataset/' + tablename + '.csv')

    return {'minIdx':minIdx-1, # -1 due to the indexing starts from 0
            'maxIdx':maxIdx-1,
            'tablename':tablename,
            }


if __name__ == "__main__":
    try:
        
        console()

    except (KeyboardInterrupt, SystemExit):
        pass