# SINTA-JMI: web_crawl

----
## Summary

This repo is a collection of the web crawl codes and the dataset.

---

## Table of Folders 


- [dataset](https://git.ecdf.ed.ac.uk/jmi/sinta-jmi/tree/master/web_crawl/dataset): contains different datasets
- [log](https://git.ecdf.ed.ac.uk/jmi/sinta-jmi/tree/master/web_crawl/log): contains logs


---

## Table of Contents 


- [sinta\_name\_id.py](https://git.ecdf.ed.ac.uk/jmi/sinta-jmi/blob/master/web_crawl/sinta_name_id.py): a script to extract list of authors and their IDs from [http://sinta.ristekbrin.go.id/authors](http://sinta.ristekbrin.go.id/authors). Running this script will generate './dataset/sinta\_name\_id\_datetime.csv' and './dataset/sinta\_name\_id\_datetime.log'
```
python sinta_name_id.py --min-page-number 1 --max-page-number 20 
```
- [sinta\_score.py](https://git.ecdf.ed.ac.uk/jmi/sinta-jmi/blob/master/web_crawl/sinta_score.py): a script to extract list of scores of the authors from, for example, [http://sinta.ristekbrin.go.id/authors/score?id=5977531](http://sinta.ristekbrin.go.id/authors/score?id=5977531). Running this script will generate './dataset/sinta\_score\_datetime.csv' and './dataset/sinta\_score\_datetime.log'
```
python sinta_score.py --tablename sinta_name_id_09062020232959 --min-idx 1 --max-idx 20  
```
- [sinta\_pub.py](https://git.ecdf.ed.ac.uk/jmi/sinta-jmi/blob/master/web_crawl/sinta_pub.py): a script to extract list of publications of the authors from, for example, [http://sinta.ristekbrin.go.id/authors/detail?page=1&id=3731&view=documentsgs](http://sinta.ristekbrin.go.id/authors/detail?page=1&id=3731&view=documentsgs). Running this script will generate './dataset/sinta\_pub\_datetime.csv' and './dataset/sinta\_pub\_datetime.log'
```
python sinta_pub.py --tablename sinta_name_id_09062020232959 --min-idx 1 --max-idx 20
``` 
- [sinta\_scopus.py](https://git.ecdf.ed.ac.uk/jmi/sinta-jmi/blob/master/web_crawl/sinta_scopus.py): a script to extract list of publications of the authors from, for example, [http://sinta.ristekbrin.go.id/authors/detail?page=1&id=3731&view=documentsgs](http://sinta.ristekbrin.go.id/authors/detail?page=1&id=3731&view=documentsscopus). Running this script will generate './dataset/sinta\_scopus\_datetime.csv' and './dataset/sinta\_scopus\_datetime.log'
```
python sinta_scopus.py --tablename sinta_name_id_09062020232959 --min-idx 1 --max-idx 20
``` 

---
## Notes

- While running the scripts, I typically monitor the logs realtime.
```
ls -t ./log | head -n 1 | xargs -I {} sh -c "tail -f ./log/{}"
```

----
## Contributors

* **Ardimas Purwita** - *Initial work* - ardimasandipurwita@outlook.com