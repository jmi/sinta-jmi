"""
    ex: python sinta_name_id_science.py --min-page-number 1 --max-page-number 20 
"""


import sys
import os
import string

import requests
from bs4 import BeautifulSoup
import re
from tqdm import tqdm

import logging
from datetime import datetime

from collections.abc import Iterable
import itertools
import csv

"""
This initial version is written by Ardimas
"""
__version__ = 0.1 

__min_page_number__ = 1
__max_page_number__ = 19811

__timeout__ = 20 # in s

__usage__ = """Usage: sinta_name_id_science [options] ...
Scraping 'name' and 'sinta_id' from SINTA website ('http://sinta.ristekbrin.go.id/authors')
Options:
  --min-page-number             set the minimum page number, default = 1
  --max-page-number             set the maximum page number, default = 19811
  -h, --help                    show this help message
  -v, --version                 show version of this command
"""

# change the following if the url is modified
# http://sinta.ristekbrin.go.id/authors?page=19806&sort=year2&view=
authors_list_page_prefix = 'http://sinta.ristekbrin.go.id/authors?page='
authors_list_page_postfix = '&sort=year2&view=science'

# create a log file
now = datetime.now()
dt_string = now.strftime("%d%m%Y%H%M%S")

# check wheter the log folder exists
# if not create one
if not os.path.exists(os.getcwd() + '/log'):
    os.makedirs(os.getcwd() + '/log')

logging.basicConfig(filename=os.getcwd() +'/log/sinta_name_id_science_'+dt_string+'.log',level=logging.DEBUG)

class ArgumentError(Exception):
    pass

def console():
    try:
        outputParsing = parse_args()

        # to avoid duplicated data
        auth_id_prev = 'whatever'

        with open(os.getcwd() + '/dataset/sinta_name_id_science_'+dt_string+'.csv', mode='w') as csv_file:
            author_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            author_writer.writerow(['page_num','name','sinta_id','scopus_h_index','google_h_index'])

            for page_idx in tqdm(range(outputParsing['minPn'],outputParsing['maxPn']+1)): # recall open right interval in python
                url = authors_list_page_prefix + str(page_idx) + authors_list_page_postfix
            
                try:

                    try:
                        page = requests.get(url, timeout=__timeout__)
                    except requests.exceptions.Timeout as err:
                        # try one more time 
                        page = requests.get(url, timeout=__timeout__)


                    soup = BeautifulSoup(page.content, 'html.parser')

                    # get names
                    # NOTE: the attribute text-blue is a subject to change in the future
                    tables = soup.find_all(class_='text-blue')

                    # get all h-indexes
                    tmp_list_str = re.findall(r'H-Index . \d*\d',soup.text)

                    tmp_h_indexes = [re.findall(r'\d*\d',tmp_str) for tmp_str in tmp_list_str]

                    scopus_h_index = tmp_h_indexes[0::2]
                    google_h_index = tmp_h_indexes[1::2]

                    for idx,item in enumerate(tables):

                        # get the name of the author
                        auth_name = item.contents
                        auth_name = auth_name[0]

                        # get the id of the author
                        tmp = re.findall(r'\d*\d',item.attrs['href'])
                        if tmp:
                            auth_id = tmp[0]
                        else:
                            auth_id = '-1' # not valid

                        logging.debug(str(page_idx))
                        logging.debug(auth_name)
                        logging.debug(auth_id)

                        # write the csv file with the following
                        # ['page_num','name','sinta_id','scopus_h_index','google_h_index']
                        author_writer.writerow([str(page_idx),auth_name,auth_id,scopus_h_index[idx][0],google_h_index[idx][0]])

                        if auth_id.lower() == auth_id_prev.lower():
                            logging.warning('There is a duplicated data with the id of ' + auth_id)
                            
                        auth_id_prev = auth_id

                except:
                    logging.warning(
                        'The author page number ' + str(page_idx) + ' is not loaded properly!')


    except(ArgumentError):
        sys.stderr.write(__usage__)

def parse_args():

    args = sys.argv[1:]

    minPn = __min_page_number__;
    maxPn = __max_page_number__; 

    # check wheter the dataset folder exists
    # if not create one
    if not os.path.exists(os.getcwd() + '/dataset'):
        os.makedirs(os.getcwd() + '/dataset')
        logging.warning("Create a folder named '%s' ", 'dataset')

    i = 0
    while i < len(args):
        op = args[i]
        if op in ("-h","--help"):
            raise ArgumentError
        if op in ("-v","--version"):
            print('Version: '+ str(__version__))
        elif op in ("--min-page-number"):
            minPn = int(args[i+1])
            i += 1
        elif op in ("--max-page-number"):
            maxPn = int(args[i+1])
            i += 1
        i += 1

    if (minPn < 1) | (maxPn > __max_page_number__):
        logging.error('page number must be between ' + str(__min_page_number__) + ' and ' + str(__max_page_number__))
        raise ArgumentError

    logging.info('page number:' + str(minPn) + '-' + str(maxPn))

    return {'minPn':minPn,
            'maxPn':maxPn}


if __name__ == "__main__":
    try:
        
        console()

    except (KeyboardInterrupt, SystemExit):
        pass
