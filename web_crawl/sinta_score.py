"""
    ex: python sinta_score.py --tablename sinta_name_id_09062020232959 --min-idx 1 --max-idx 20 
"""


import sys
import os
import string

import requests
from bs4 import BeautifulSoup
import re
from tqdm import tqdm

import logging
from datetime import datetime

from collections.abc import Iterable
import itertools
import csv

import pandas as pd

"""
This initial version is written by Ardimas
"""
__version__ = 0.1 

__min_idx__ = 1

__timeout__ = 20 # in s

__usage__ = """Usage: sinta_score [options] ...
Scraping scores from SINTA website
Options:
  --tablename                   pick the table name in './dataset/', ex: sinta_name_id_09062020232959
  --min-idx                     set the minimum index, default = 1
  --max-idx                     set the maximum index, default depends on the tablename
  -h, --help                    show this help message
  -v, --version                 show version of this command
"""

# http://sinta.ristekbrin.go.id/authors/score?id=5977531
__authors_score_page_prefix__ = 'http://sinta.ristekbrin.go.id/authors/score?id='

# see http://sinta.ristekbrin.go.id/authors/score?id=5974510
__list_parameters_V2_Overall__ = [
    "Publikasi Scopus Q1 (Bobot)",
    "Publikasi Scopus Q1 (Value)",
    "Publikasi Scopus Q1 (Score)",
    "Publikasi Scopus Q2 (Bobot)",
    "Publikasi Scopus Q2 (Value)",
    "Publikasi Scopus Q2 (Score)",
    "Publikasi Scopus Q3 (Bobot)",
    "Publikasi Scopus Q3 (Value)",
    "Publikasi Scopus Q3 (Score)",
    "Publikasi Scopus Q4 (Bobot)",
    "Publikasi Scopus Q4 (Value)",
    "Publikasi Scopus Q4 (Score)",
    "Publikasi Scopus No-Q (Bobot)",
    "Publikasi Scopus No-Q (Value)",
    "Publikasi Scopus No-Q (Score)",
    "Publikasi Scopus Non Jurnal (Scopus Conference, Scopus Book) (Bobot)", 
    "Publikasi Scopus Non Jurnal (Scopus Conference, Scopus Book) (Value)", 
    "Publikasi Scopus Non Jurnal (Scopus Conference, Scopus Book) (Score)", 
    "Sitasi Scopus (Bobot)", 
    "Sitasi Scopus (Value)", 
    "Sitasi Scopus (Score)", 
    "Sitasi Google (Bobot)", 
    "Sitasi Google (Value)", 
    "Sitasi Google (Score)", 
    "Publikasi di Jurnal terakreditasi S1 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S1 (Value)", 
    "Publikasi di Jurnal terakreditasi S1 (Score)", 
    "Publikasi di Jurnal terakreditasi S2 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S2 (Value)", 
    "Publikasi di Jurnal terakreditasi S2 (Score)", 
    "Publikasi di Jurnal terakreditasi S3 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S3 (Value)", 
    "Publikasi di Jurnal terakreditasi S3 (Score)", 
    "Publikasi di Jurnal terakreditasi S4 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S4 (Value)", 
    "Publikasi di Jurnal terakreditasi S4 (Score)", 
    "Publikasi di Jurnal terakreditasi S5 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S5 (Value)", 
    "Publikasi di Jurnal terakreditasi S5 (Score)", 
    "Publikasi di Jurnal terakreditasi S6 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S6 (Value)", 
    "Publikasi di Jurnal terakreditasi S6 (Score)", 
    "Total Sinta Score V2 Overall"
    ]

__list_parameters_V2_2017d2019__ = [
    "Publikasi Scopus Q1 (Bobot)",
    "Publikasi Scopus Q1 (Value)",
    "Publikasi Scopus Q1 (Score)",
    "Publikasi Scopus Q2 (Bobot)",
    "Publikasi Scopus Q2 (Value)",
    "Publikasi Scopus Q2 (Score)",
    "Publikasi Scopus Q3 (Bobot)",
    "Publikasi Scopus Q3 (Value)",
    "Publikasi Scopus Q3 (Score)",
    "Publikasi Scopus Q4 (Bobot)",
    "Publikasi Scopus Q4 (Value)",
    "Publikasi Scopus Q4 (Score)",
    "Publikasi Scopus No-Q (Bobot)",
    "Publikasi Scopus No-Q (Value)",
    "Publikasi Scopus No-Q (Score)",
    "Publikasi Scopus Non Jurnal (Scopus Conference, Scopus Book) (Bobot)", 
    "Publikasi Scopus Non Jurnal (Scopus Conference, Scopus Book) (Value)", 
    "Publikasi Scopus Non Jurnal (Scopus Conference, Scopus Book) (Score)", 
    "Sitasi Scopus (Bobot)", 
    "Sitasi Scopus (Value)", 
    "Sitasi Scopus (Score)", 
    "Sitasi Google (Bobot)", 
    "Sitasi Google (Value)", 
    "Sitasi Google (Score)", 
    "Publikasi di Jurnal terakreditasi S1 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S1 (Value)", 
    "Publikasi di Jurnal terakreditasi S1 (Score)", 
    "Publikasi di Jurnal terakreditasi S2 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S2 (Value)", 
    "Publikasi di Jurnal terakreditasi S2 (Score)", 
    "Publikasi di Jurnal terakreditasi S3 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S3 (Value)", 
    "Publikasi di Jurnal terakreditasi S3 (Score)", 
    "Publikasi di Jurnal terakreditasi S4 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S4 (Value)", 
    "Publikasi di Jurnal terakreditasi S4 (Score)", 
    "Publikasi di Jurnal terakreditasi S5 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S5 (Value)", 
    "Publikasi di Jurnal terakreditasi S5 (Score)", 
    "Publikasi di Jurnal terakreditasi S6 (Bobot)", 
    "Publikasi di Jurnal terakreditasi S6 (Value)", 
    "Publikasi di Jurnal terakreditasi S6 (Score)", 
    "Total Sinta Score V2 3 Year"
    ]

# create a log file
now = datetime.now()
dt_string = now.strftime("%d%m%Y%H%M%S")

# check wheter the log folder exists
# if not create one
if not os.path.exists(os.getcwd() + '/log'):
    os.makedirs(os.getcwd() + '/log')

logging.basicConfig(filename='./log/sinta_score_'+dt_string+'.log',level=logging.DEBUG)

def flatten(arr):
    """ Flatten a messy iterable object into 
    an 1D iterable object.
    Examples
    --------
    >>> x = [[1,2],[1],[1,2,3,[4,5]]]
    >>> y = list(flatten(x))
    [1,2,1,1,2,3,4,5]
    
    """
    for i in arr:
        if isinstance(i,Iterable) and not isinstance(i,str):
            yield from flatten(i)
        else:
            yield i

class ArgumentError(Exception):
    pass

def console():
    try:
        outputParsing = parse_args()

        table = pd.read_csv(os.getcwd() +'/dataset/' + outputParsing['tablename'] + '.csv')
        num_rows = len(table.index)

        with open(os.getcwd() + '/dataset/sinta_score_'+dt_string+'.csv', mode='w') as csv_file:
            author_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            author_writer.writerow(list(flatten(['rank','name','sinta_id','affil','focus','url',__list_parameters_V2_Overall__,__list_parameters_V2_2017d2019__])))

            for idy in tqdm(range(outputParsing['minIdx'],outputParsing['maxIdx']+1)):

                auth_id = table['sinta_id'][idy]
                auth_name = table['name'][idy]

                # get the url for author's score list
                url_page = __authors_score_page_prefix__ + str(auth_id)

                try:
                    page = requests.get(url_page, timeout=__timeout__)
                except requests.exceptions.Timeout as err:
                    # try one more time 
                    page = requests.get(url_page, timeout=__timeout__)

                soup = BeautifulSoup(page.content, 'html.parser')

                tables = soup.find_all(class_='uk-text-center')
                tables = tables[3:95] # NOTE: The numbers 3 and 95 are hard-coded
                
                SintaScoreV2Overall_raw = [tables[idx].contents[0] for idx in range(3,46)]
                SintaScoreV22017d2019_raw = [tables[idx].contents[0] for idx in range(49,92)]

                affil = soup.find_all(class_='au-affil')
                strtmp = re.findall(r'view">.*</a',str(affil[0]))
                affil = strtmp[0][strtmp[0].find('>')+1:strtmp[0].find('<')]


                badges = soup.find_all(class_='uk-badge')
                badges = [badge.contents[0] for badge in badges]
                
                stats = soup.find_all(class_='stat2-val')
                rank_verified = stats[4].contents[0];

                name_ver = soup.find_all(class_='au-name')
                name_ver = name_ver[0].contents[0]

                author_writer.writerow(
                    list(flatten([rank_verified,auth_name,auth_id,affil,', '.join(badges),url_page,SintaScoreV2Overall_raw,SintaScoreV22017d2019_raw])))



    except(ArgumentError):
        sys.stderr.write(__usage__)

def parse_args():

    args = sys.argv[1:]

    minIdx = __min_idx__;

    # check wheter the dataset folder exists
    # if not create one
    if not os.path.exists(os.getcwd() + '/dataset'):
        os.makedirs(os.getcwd() + '/dataset')
        logging.warning("Create a folder named '%s' ", 'dataset')

    i = 0
    while i < len(args):
        op = args[i]
        if op in ("-h","--help"):
            raise ArgumentError
        if op in ("-v","--version"):
            print('Version: '+ str(__version__))
        elif op in ("--tablename"):
            tablename = args[i+1]
            i += 1
        elif op in ("--min-idx"):
            minIdx = int(args[i+1])
            i += 1
        elif op in ("--max-idx"):
            maxIdx = int(args[i+1])
            i += 1
        i += 1

    try:
        table = pd.read_csv(os.getcwd() +'/dataset/' + tablename + '.csv')
    except:
        errorMsg = os.getcwd() +'/dataset/' + tablename + '.csv' + ' does not exist!!'
        print(errorMsg)
        logging.error(errorMsg)
        raise ArgumentError

    if 'maxIdx' not in locals():
        maxIdx = len(table.index)

    if (minIdx < 1) | (maxIdx > len(table.index)):
        logging.error('page number must be between ' + str(__min_idx__) + ' and ' + str(len(table.index)))
        raise ArgumentError

    logging.info('index number:' + str(minIdx) + '-' + str(maxIdx))
    logging.info('tablename: ' + os.getcwd() +'/dataset/' + tablename + '.csv')

    return {'minIdx':minIdx-1, # -1 due to the indexing starts from 0
            'maxIdx':maxIdx-1,
            'tablename':tablename,
            }


if __name__ == "__main__":
    try:
        
        console()

    except (KeyboardInterrupt, SystemExit):
        pass