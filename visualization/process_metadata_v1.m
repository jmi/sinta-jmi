%% Process metadata_500authors.json v1
% by: Ardimas Purwita (ardimasandipurwita@outlook.com)

%% Clear variables and close all figures
clear 
close all

% Open file
metadata = jsondecode(fileread('./../dataset/metadata_500authors.json'));

% Get the field names
field = fieldnames(metadata);

% Define the range of years
% years_range.T1 = [2020:-1:2017];
% years_range.T2 = [2016:-1:2014];
% years_range.T3 = [2013:-1:2011];
% years_range.T4 = [2010:-1:2008];
% years_range.T5 = [2007:-1:2005];
% years_range.T6 = [2004:-1:2002];
% years_range.T7 = [2001:-1:1999];
% years_range.T8 = [1998:-1:0];

% years_range.T1 = [2020:-1:2016];
% years_range.T2 = [2015:-1:2012];
% years_range.T3 = [2011:-1:2008];
% years_range.T4 = [2007:-1:2004];
% years_range.T5 = [2003:-1:2000];
% years_range.T6 = [1999:-1:0];

% years_range.T1 = [2020:-1:2017];
% years_range.T2 = [2016:-1:2010];
% years_range.T3 = [2009:-1:2000];
% years_range.T4 = [1999:-1:0];

% years_range.T1 = [2020:-1:2016];
% years_range.T2 = [2015:-1:2011];
% years_range.T3 = [2010:-1:2006];
% years_range.T4 = [2005:-1:2001];
% years_range.T5 = [2000:-1:0];

% years_range.T1 = [2020:-1:2011];
% years_range.T2 = [2010:-1:2001];
% years_range.T3 = [2000:-1:0];

years_range.T1 = [2020:-1:2017];
years_range.T2 = [2016:-1:2013];
years_range.T3 = [2012:-1:2009];
years_range.T4 = [2008:-1:2005];
years_range.T5 = [2004:-1:2001];
years_range.T6 = [2000:-1:0];

% Bonus to calculate the authorship
bonus = 0.2;

% the r-th author
for r = 1:length(field)
    
    % Get the data and  fieldname of the r-th author
    data_rth = getfield(metadata,field{r});
    field_data_rth = fieldnames(data_rth);
    length_pub_rth  = length(field_data_rth)-2;

    v_p_r = zeros(length_pub_rth,1);
        
    % initialize P_r
    P_r = years_range;
    field_P_r = fieldnames(P_r);
    for j = 1:length(field_P_r)
        P_r.(field_P_r{j}) = 0;
    end

    % Only process if the r-th author has at least a publication
    if length_pub_rth > 0

        % Lopp over the list of publications
        for i_p_r = 1:length_pub_rth
            % The p-th publication of the r-th author
            p_r = getfield(data_rth,field_data_rth{2+i_p_r}); % the addition of two is due to the first two fileds are allocated for name and url

            % Calculate the authorship based on bonus and the information of the list of authors            
            authorship = calc_authorship(bonus,p_r);

            % handle mislabel
            % NOTE: there are a few bugs in parsing the pub_year. manually handle them instead.
            if r == 168 & i_p_r== 26 
                p_r.pub_year = '2013'; % http://sinta.ristekbrin.go.id/authors/detail?page=3&id=6657222&view=documentsscopus
            elseif r == 168 & i_p_r== 34 
                p_r.pub_year = '2012'; % http://sinta.ristekbrin.go.id/authors/detail?page=4&id=6657222&view=documentsscopus
            elseif r == 168 & i_p_r== 49 
                p_r.pub_year = '2001'; % http://sinta.ristekbrin.go.id/authors/detail?page=4&id=6657222&view=documentsscopus
            elseif r == 255 & i_p_r== 17 
                p_r.pub_year = '2012'; % http://sinta.ristekbrin.go.id/authors/detail?page=2&id=6022454&view=documentsscopus
            elseif r == 357 & i_p_r== 29 
                p_r.pub_year = '2012'; % http://sinta.ristekbrin.go.id/authors/detail?page=4&id=6025740&view=documentsscopus
            elseif r == 383 & i_p_r== 34 
                p_r.pub_year = '2013'; % http://sinta.ristekbrin.go.id/authors/detail?page=5&id=6038528&view=documentsscopus
            end

            if str2num(p_r.pub_year) == 2020
                % During the time of writing, the csv files from SCIMAGOJR are only available for up to 2019
                % So at least assume the SJRs do not change significantly compared to those in 2019.
                scimagojr = getfield(p_r.scimagojr,['x','2019']);
            elseif str2num(p_r.pub_year) < 1999
                % During the time of writing, the csv files from SCIMAGOJR are only available from 1919
                % So at most assume the SJRs are the same with those in 1999
                scimagojr = getfield(p_r.scimagojr,['x','1999']);
            else
                % Get the most up-to-date SJR value
                % scimagojr = getfield(p_r.scimagojr,['x',p_r.pub_year]);
                pub_year = str2num(p_r.pub_year);
                for py = 2019:-1:pub_year
                    scimagojr = getfield(p_r.scimagojr,['x',num2str(py)]);

                    score_pub = str2num(replace(scimagojr.sjr,',','.'));
                    if ~(isempty(score_pub) | isnan(score_pub))
                        break
                    end
                end
            end
            
            % Data from the csv value from SCIMAGOJR use a comma as a delimiter for a decimal point
            score_pub = str2num(replace(scimagojr.sjr,',','.'));
            if isempty(score_pub) | isnan(score_pub)
                score_pub = 0;
            end

            % authorship*(sjr+1)*(#citations+1)
            % v_p_r(i_p_r) = authorship*score_pub;
            v_p_r(i_p_r) = authorship*(score_pub+1)*(str2num(p_r.citation_num)+1);

            % A hard stop for a nan value
            if isnan(v_p_r(i_p_r))
                stop
            end

            % Aggregate
            for j = 1:length(field_P_r)
                if ismember(str2num(p_r.pub_year),years_range.(field_P_r{j}))
                    P_r.(field_P_r{j}) = P_r.(field_P_r{j}) + v_p_r(i_p_r);
                    break
                end
            end

        end

        P.(field{r}) = P_r;

    else
        P.(field{r}) = P_r;
    end

end



function authorship = calc_authorship(b,p_r)
    if p_r.is_in_et_al % if the author is in the 'et al' list
        N = p_r.tot_author + 1; % the total number of authors
        o = p_r.order_author + 1; % the order of the authors
    else
        N = p_r.tot_author; % the total number of authors
        o = p_r.order_author; % the order of the authors
    end

    % Due to a_prev that can be negative, a might be negative as well

    a = zeros(N,1);
    for i = 1:N-1
        a_prev = 1-sum(a(1:i-1));
        
        a(i) = (1/(N-(i-1)))*( double((a_prev)-b >= 0)*((a_prev)-b)+double((a_prev)-b < 0)*((a_prev)) )+b*double((a_prev)-b >= 0);
        a(i+1:end) = (1/(N-(i-1)))*(double((a_prev)-b >= 0)*((a_prev)-b)+double((a_prev)-b < 0)*((a_prev)));

    end

    authorship = a(o);

end
