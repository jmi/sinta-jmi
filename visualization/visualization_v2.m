%% Draw visualizations for dataset v2
% by: Ardimas Purwita (ardimasandipurwita@outlook.com)

%% Clear variables and close all figures
clear
close all

% area = {'art','health','science','social'}
% area = 'science';
array_area = {'art','health','science','social'};

for id = 1:4
    area = array_area{id};
    fprintf('area: %s\n',area);
    if strcmp(area,'art')
        load cell_authorsip_art.mat
        sinta_pub = jsondecode(fileread('./../dataset/v2_rev/final/sinta_pub_art.json'));
        sinta_scopus = jsondecode(fileread('./../dataset/v2_rev/final/sinta_scopus_art.json'));
    elseif strcmp(area,'health')
        load cell_authorsip_health.mat
        sinta_pub = jsondecode(fileread('./../dataset/v2_rev/final/sinta_pub_health.json'));
        sinta_scopus = jsondecode(fileread('./../dataset/v2_rev/final/sinta_scopus_health.json'));
    elseif strcmp(area,'science')
        load cell_authorsip_science.mat
        sinta_pub = jsondecode(fileread('./../dataset/v2_rev/final/sinta_pub_science.json'));
        sinta_scopus = jsondecode(fileread('./../dataset/v2_rev/final/sinta_scopus_science.json'));
    elseif strcmp(area,'social')
        load cell_authorsip_social.mat
        sinta_pub = jsondecode(fileread('./../dataset/v2_rev/final/sinta_pub_social.json'));
        sinta_scopus = jsondecode(fileread('./../dataset/v2_rev/final/sinta_scopus_social.json'));
    else
        error('undefined!')
    end
        
    field_sinta_pub = fieldnames(sinta_pub);
    field_sinta_scopus = fieldnames(sinta_scopus);


    b = 0.2; %bonus
    authorships = zeros(length(cell_authorsip),1);
    for idx = 1:length(cell_authorsip)
        % loop through items where there is an intersection between publications in GS and Scopus
        indexes = find(cell_authorsip{idx}.list_totauthors ~= 0);
        if indexes 
            for idy = 1:length(indexes) 
                if cell_authorsip{idx}.list_isinetal(indexes(idy)) % if the author is in the 'et al' list
                    N = cell_authorsip{idx}.list_totauthors(indexes(idy)) + 1; % the total number of authors
                    o = cell_authorsip{idx}.list_order(indexes(idy)) + 1; % the order of the authors
                else
                    N = cell_authorsip{idx}.list_totauthors(indexes(idy)); % the total number of authors
                    o = cell_authorsip{idx}.list_order(indexes(idy)); % the order of the authors
                end

                % Due to a_prev that can be negative, a might be negative as well
                % a = zeros(N,1);
                % for i = 1:N-1
                %     a_prev = 1-sum(a(1:i-1));
                    
                %     a(i) = (1/(N-(i-1)))*((a_prev)-b)+b;
                %     a(i+1:end) = (1/(N-(i-1)))*((a_prev)-b);

                % end

                a = zeros(N,1);
                for i = 1:N-1
                    a_prev = 1-sum(a(1:i-1));
                    
                    a(i) = (1/(N-(i-1)))*( double((a_prev)-b >= 0)*((a_prev)-b)+double((a_prev)-b < 0)*((a_prev)) )+b*double((a_prev)-b >= 0);
                    a(i+1:end) = (1/(N-(i-1)))*(double((a_prev)-b >= 0)*((a_prev)-b)+double((a_prev)-b < 0)*((a_prev)));

                end

                authorships(idx) = authorships(idx)+a(o);
                % assert(a(o)>=0)
                % assert(~isnan(a(o)))
            end
            authorships(idx) = authorships(idx)/length(indexes) ;
            assert(authorships(idx) >= 0 & authorships(idx) <= 1)
        end
    end

    figure
    index = [1:500]';
    stem(index,authorships,'LineStyle','none')
    xlabel('Author rank per area')
    ylabel('Average authorship')
    grid on
    set(gca,'FontSize',24)
    set(gca,'FontName','Times New Roman')
    pbaspect([2,1,1])
    ylim([0,1])
    xlim([1,500])

    % saveas(gcf,['./rawfigs_v2/authorship_', area, '.fig'])
    % saveas(gcf,['./rawfigs_v2/authorship_', area, '.pdf'])

    disp(join(["> The Pearson's correlation coeff. of authorships and rank is" num2str(mean(diag(fliplr(corrcoef(index,authorships)))))]))
    disp(['> mean: ' num2str(mean(authorships))])

    figure
    histogram(authorships,'BinWidth',0.02)
    xlabel('Average authorship')
    ylabel('Frequency')
    grid on
    set(gca,'FontSize',24)
    set(gca,'FontName','Times New Roman')
    pbaspect([2,1,1])
    xlim([0,1])

    % saveas(gcf,['./rawfigs_v2/hist_authorship_', area, '.fig'])
    % saveas(gcf,['./rawfigs_v2/hist_authorship_', area, '.pdf'])
end