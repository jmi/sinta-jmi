%% Data Visualization for Dataset v1
% by: Ardimas Purwita (ardimasandipurwita@outlook.com)

%% Clear variables and close all figures
clear
close all

%% Open files
% NOTE: Remember that this code is mainly dedicated for the first version!!!
sinta_pub = jsondecode(fileread('./../dataset/sinta_pub_v1.json'));
sinta_scopus = jsondecode(fileread('./../dataset/sinta_scopus_v1.json'));
sinta_score = readtable('./../dataset/sinta_score_v1.csv');
sinta_name_id = readtable('./../dataset/sinta_name_id_v1.csv');
scimagojr = jsondecode(fileread('./../dataset/scimagojr_v1.json'));

% There will be a warning as the column names for the overal and latest-3-year criterions are the same
% The warning is as follows, and we can ignore it for the time being.
% Warning: Column headers from the file
% were modified to make them valid MATLAB
% identifiers before creating variable
% names for the table. The original column
% headers are saved in the
% VariableDescriptions property.
% Set 'PreserveVariableNames' to true to
% use the original column headers as table
% variable names.

%%  Correlation of rank and authorship
field_sinta_pub = fieldnames(sinta_pub);
field_sinta_scopus = fieldnames(sinta_scopus);
field_scimagojr = fieldnames(scimagojr);


% load('cell_authorsip.mat')
% The below routines generate the .mat above
% Beware of the time it takes 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% uncomment from here
cell_authorsip = {};

waitingBarDialog = waitbar(0,'1',...
    'CreateCancelBtn','delete(waitingBarDialog);');
try
    for id = 1:length(field_sinta_scopus) % loop for all authors' id

        waitbar(id/length(field_sinta_scopus),waitingBarDialog,sprintf('%d of %d',id,length(field_sinta_scopus)))

        ith_author_scopus = getfield(sinta_scopus,field_sinta_scopus{id});
        ith_author_gs = getfield(sinta_pub,field_sinta_pub{id});

        if length(ith_author_scopus) > 1
            names = split(ith_author_scopus{1}.name,' '); % name is the first field
        else % some authors have 0 page on, for example, http://sinta.ristekbrin.go.id/authors/detail?id=6719997&view=documentsscopus
            names = split(ith_author_scopus.name,' '); % name is the first field
        end

        if length(names) == 1 % firstname = surname
            surname = names{end}; 
            firstname = surname;
        else
            firstname = names{1};
            surname = names{end}; 
        end

       % list_indexes contains indexes of publication in GS that matches pub in Scopus
        list_indexes = zeros(length(ith_author_scopus),1);
        % list_order contains the order of the author
        list_order = zeros(length(ith_author_scopus),1);
        list_totauthors = zeros(length(ith_author_scopus),1);
        list_isalphabetical = zeros(length(ith_author_scopus),1);
        list_isinetal = zeros(length(ith_author_scopus),1); % is the author in the et. al.
        list_isnotlisted = zeros(length(ith_author_scopus),1); % is the author is not even listed in the list

        for idx = 2:length(ith_author_scopus)
            
            for idy = 2:length(ith_author_gs)
                
                tmp_surnames = {};
                list_authors = split(ith_author_gs{idy}.authors,', ');
                for idz=1:length(list_authors)
                    surnames_list_authors = split(list_authors{idz});
                    surnames_list_authors = surnames_list_authors{end};
                    if ~strcmp(lower(surnames_list_authors),'...') 
                        tmp_surnames{end+1} = lower(surnames_list_authors);
                    end
                end
                if isequal(sort(tmp_surnames),tmp_surnames) % alphabetically ordered
                    is_alphabetically_ordered = 1;
                else 
                    is_alphabetically_ordered = 0;
                end
                

                if ~isempty(strfind(...
                    lower(ith_author_gs{idy}.title(regexp(lower(ith_author_gs{idy}.title),'[a-z]'))),...
                    lower(ith_author_scopus{idx}.title(regexp(lower(ith_author_scopus{idx}.title),'[a-z]')))...
                    ))

                    list_indexes(idx,1) = idy;
                    comma_pos = strfind(lower(ith_author_gs{idy}.authors),',');
                    tmporder = strfind(lower(ith_author_gs{idy}.authors),lower(surname));

                    if isempty(tmporder) % the author is listed within et.al. or ... or not in the list
                        if isempty(strfind(lower(ith_author_gs{idy}.authors),'...'))
                            list_order(idx,1) = 0;
                            list_totauthors(idx,1) = 0;
                            list_isinetal(idx,1) = 0;
                            list_isnotlisted(idx,1) = 1;
                        else
                            list_order(idx,1) = length(comma_pos);
                            list_totauthors(idx,1) = length(comma_pos);
                            list_isinetal(idx,1) = 1;
                            list_isnotlisted(idx,1) = 0;
                        end
                    else
                        list_isnotlisted(idx,1) = 0;
                        if isempty(comma_pos) % single author
                            list_order(idx,1) = 1;
                            list_totauthors(idx,1) = 1;
                        else
                            % tmporder should have the length of 1.
                            % But see http://sinta.ristekbrin.go.id/authors/detail?q=Real-Time+Electroencephalography-Based+Emotion+Recognition+System&search=1&id=29555&view=documentsgs
                            % the names in the list of author are duplicated

                            % proper search
                            list_authors = split(ith_author_gs{idy}.authors,', ');
                            
                            for idz=1:length(list_authors)
                                if ~isempty(strfind(lower(list_authors{idz}),lower(surname) ))
                                    if ~strcmp(surname,firstname)
                                        initial_name = lower(list_authors{idz});
                                        initial_name = initial_name(1);
                                        % this won't work for example HO Sinaga
                                        % sometimes the list says O Sinaga, sometimes it says HO Sinaga
                                        % if lower(firstname(1)) == initial_name
                                        %     order_author = idz;
                                        % end
                                        
                                        % revise
                                        % naively assign it
                                        order_author = idz;
                                    else
                                        order_author = idz;
                                    end
                                    break % use the first match only
                                end
                            end

                            list_order(idx,1) = order_author;
                            list_isalphabetical(idx,1) = is_alphabetically_ordered;

                            %% lazy search
                            % tmp = find(tmporder(1)>comma_pos); % in order to avoid the duplicated name

                            % if isempty(tmp) % first author
                            %     list_order(idx,1) = 1;
                            % else
                            %     list_order(idx,1) = max(tmp)+1;
                            % end

                            list_totauthors(idx,1) = length(comma_pos)+1;
                            assert(list_order(idx,1) <= list_totauthors(idx,1))
                        end
                    end
                end
            end
        end

        tmpstruct.list_indexes = list_indexes;
        tmpstruct.list_order = list_order;
        tmpstruct.list_totauthors = list_totauthors;
        tmpstruct.list_isalphabetical = list_isalphabetical;
        tmpstruct.list_isinetal = list_isinetal;
        cell_authorsip{end+1} = tmpstruct;

    end

    delete(waitingBarDialog)
catch ME
    delete(waitingBarDialog)
    rethrow(ME)
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% uncomment to here
stop

% b = 0.2; %bonus
% authorships = zeros(length(cell_authorsip),1);
% for idx = 1:length(cell_authorsip)
%     % loop through items where there is an intersection between publications in GS and Scopus
%     indexes = find(cell_authorsip{idx}.list_totauthors ~= 0);
%     if indexes 
%         for idy = 1:length(indexes) 
%             if cell_authorsip{idx}.list_isinetal(indexes(idy)) % if the author is in the 'et al' list
%                 N = cell_authorsip{idx}.list_totauthors(indexes(idy)) + 1; % the total number of authors
%                 o = cell_authorsip{idx}.list_order(indexes(idy)) + 1; % the order of the authors
%             else
%                 N = cell_authorsip{idx}.list_totauthors(indexes(idy)); % the total number of authors
%                 o = cell_authorsip{idx}.list_order(indexes(idy)); % the order of the authors
%             end

%             % Due to a_prev that can be negative, a might be negative as well
%             % a = zeros(N,1);
%             % for i = 1:N-1
%             %     a_prev = 1-sum(a(1:i-1));
                
%             %     a(i) = (1/(N-(i-1)))*((a_prev)-b)+b;
%             %     a(i+1:end) = (1/(N-(i-1)))*((a_prev)-b);

%             % end

%             a = zeros(N,1);
%             for i = 1:N-1
%                 a_prev = 1-sum(a(1:i-1));
                
%                 a(i) = (1/(N-(i-1)))*( double((a_prev)-b >= 0)*((a_prev)-b)+double((a_prev)-b < 0)*((a_prev)) )+b*double((a_prev)-b >= 0);
%                 a(i+1:end) = (1/(N-(i-1)))*(double((a_prev)-b >= 0)*((a_prev)-b)+double((a_prev)-b < 0)*((a_prev)));

%             end

%             authorships(idx) = authorships(idx)+a(o);
%             % assert(a(o)>=0)
%             % assert(~isnan(a(o)))
%         end
%         authorships(idx) = authorships(idx)/length(indexes) ;
%         assert(authorships(idx) >= 0 & authorships(idx) <= 1)
%     end
% end

% figure
% ranking = sinta_score.rank;
% stem(ranking,authorships,'LineStyle','none')
% xlabel('Ranking')
% ylabel('Average authorship')
% grid on
% set(gca,'FontSize',24)
% set(gca,'FontName','Times New Roman')
% pbaspect([2,1,1])
% ylim([0,1])

% disp(join(["> The Pearson's correlation coeff. of authorships and rank is" num2str(mean(diag(fliplr(corrcoef(ranking,authorships)))))]))

% figure
% histogram(authorships,'BinWidth',0.02)
% xlabel('Average authorship')
% ylabel('Frequency')
% grid on
% set(gca,'FontSize',24)
% set(gca,'FontName','Times New Roman')
% pbaspect([2,1,1])
% xlim([0,1])

% SJR visualization
sjr = zeros(length(field_scimagojr),1);
for idx = 1:length(field_scimagojr)
    sjr(idx) = str2num(getfield(scimagojr,field_scimagojr{idx}).sjr);
end

figure
histogram(sjr,'BinWidth',5e-2)
xlabel('SJR')
ylabel('Frequency')
grid on
set(gca,'FontSize',24)
set(gca,'FontName','Times New Roman')
pbaspect([2,1,1])
% xlim([0,1])

% %% Show the variable names of sinta_score (comment this if not needed)
% % disp('> The variable names of sinta_score are:')
% % for idx = 1:length(sinta_score.Properties.VariableNames)
% %     tmp = sinta_score.Properties.VariableNames(idx); % it is cell
% %     disp(['- ' tmp{1}])
% % end

% %% Correlation of rank and the number of citations based on Scopus
% ranking = sinta_score.rank;
% num_scopus_citations = sinta_score.SitasiScopus_Value__1;

% figure
% stem(ranking,num_scopus_citations)
% xlabel('Ranking')
% ylabel('# of citations on Scopus')
% grid on
% set(gca,'FontSize',24)
% set(gca,'FontName','Times New Roman')
% pbaspect([4,1,1])
% set(gcf,'Position',[60 378 1273 420])

% disp(join(["> The Pearson's correlation coeff. of # of citations on Scopus and rank is" num2str(mean(diag(fliplr(corrcoef(ranking,num_scopus_citations)))))]))

% %% Correlation of rank and the number of citations based on Scopus
% ranking = sinta_score.rank;
% num_q1_scopus = sinta_score.PublikasiScopusQ1_Value__1;

% tmp_num_google_citations = sinta_score.SitasiGoogle_Value__1;
% num_google_citations = [];
% for idx = 1:length(tmp_num_google_citations)
%     tmp = tmp_num_google_citations(idx);
%     tmp2 = split(tmp{1},'/');
%     num_google_citations = [num_google_citations;str2num(tmp2{1})];
% end

% figure
% stem(ranking,num_google_citations)
% xlabel('Ranking')
% ylabel('# of citations on Google Scholar')
% grid on
% set(gca,'FontSize',24)
% set(gca,'FontName','Times New Roman')
% pbaspect([2,1,1])

% figure
% stem(ranking,num_q1_scopus)
% xlabel('Ranking')
% ylabel('# of Q1 publications on Scopus')
% grid on
% set(gca,'FontSize',24)
% set(gca,'FontName','Times New Roman')
% pbaspect([2,1,1])

% disp(join(["> The Pearson's correlation coeff. of # of citations on Google and rank is" num2str(mean(diag(fliplr(corrcoef(ranking,num_google_citations)))))]))
% disp(join(["> The Pearson's correlation coeff. of # of Q1 on Scopus and rank is" num2str(mean(diag(fliplr(corrcoef(ranking,num_q1_scopus)))))]))

