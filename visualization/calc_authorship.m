function authorship = calc_authorship(b,p_r)
    if p_r.is_in_et_al % if the author is in the 'et al' list
        N = p_r.tot_author + 1; % the total number of authors
        o = p_r.order_author + 1; % the order of the authors
    else
        N = p_r.tot_author; % the total number of authors
        o = p_r.order_author; % the order of the authors
    end

    % Due to a_prev that can be negative, a might be negative as well

    a = zeros(N,1);
    for i = 1:N-1
        a_prev = 1-sum(a(1:i-1));
        
        a(i) = (1/(N-(i-1)))*( double((a_prev)-b >= 0)*((a_prev)-b)+double((a_prev)-b < 0)*((a_prev)) )+b*double((a_prev)-b >= 0);
        a(i+1:end) = (1/(N-(i-1)))*(double((a_prev)-b >= 0)*((a_prev)-b)+double((a_prev)-b < 0)*((a_prev)));

    end

    authorship = a(o);

end