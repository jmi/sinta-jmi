%% Get cell authorship for dataset v2
% by: Ardimas Purwita (ardimasandipurwita@outlook.com)

%% Clear variables and close all figures
% clear
% close all

function get_cell_authorship_v2(area);

% area = {'all','art','health','science','social'}

if strcmp(area,'all')
    sinta_pub = jsondecode(fileread('./../dataset/v2_rev/final/sinta_pub_all.json'));
    sinta_scopus = jsondecode(fileread('./../dataset/v2_rev/final/sinta_scopus_all.json'));
elseif strcmp(area,'art')
    sinta_pub = jsondecode(fileread('./../dataset/v2_rev/final/sinta_pub_art.json'));
    sinta_scopus = jsondecode(fileread('./../dataset/v2_rev/final/sinta_scopus_art.json'));
elseif strcmp(area,'health')
    sinta_pub = jsondecode(fileread('./../dataset/v2_rev/final/sinta_pub_health.json'));
    sinta_scopus = jsondecode(fileread('./../dataset/v2_rev/final/sinta_scopus_health.json'));
elseif strcmp(area,'science')
    sinta_pub = jsondecode(fileread('./../dataset/v2_rev/final/sinta_pub_science.json'));
    sinta_scopus = jsondecode(fileread('./../dataset/v2_rev/final/sinta_scopus_science.json'));
elseif strcmp(area,'social')
    sinta_pub = jsondecode(fileread('./../dataset/v2_rev/final/sinta_pub_social.json'));
    sinta_scopus = jsondecode(fileread('./../dataset/v2_rev/final/sinta_scopus_social.json'));
else
    error('undefined!')
end
    


field_sinta_pub = fieldnames(sinta_pub);
field_sinta_scopus = fieldnames(sinta_scopus);


% Beware of the time it takes 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% uncomment from here
cell_authorsip = {};

% waitingBarDialog = waitbar(0,'1',...
%     'CreateCancelBtn','delete(waitingBarDialog);');
try
    for id = 1:length(field_sinta_scopus) % loop for all authors' id
    % for id = 1:500

        % waitbar(id/length(field_sinta_scopus),waitingBarDialog,sprintf('%d of %d',id,length(field_sinta_scopus)))
        fprintf('id=%d of %d \n',id,length(field_sinta_scopus))
        % fprintf('id=%d of %d \n',id,500)

        ith_author_scopus = getfield(sinta_scopus,field_sinta_scopus{id});
        ith_author_gs = getfield(sinta_pub,field_sinta_pub{id});

        if length(ith_author_scopus) > 1
            names = split(ith_author_scopus{1}.name,' '); % name is the first field
        else % some authors have 0 page on, for example, http://sinta.ristekbrin.go.id/authors/detail?id=6719997&view=documentsscopus
            names = split(ith_author_scopus.name,' '); % name is the first field
        end

        if length(names) == 1 % firstname = surname
            surname = names{end}; 
            firstname = surname;
        else
            firstname = names{1};
            surname = names{end}; 
        end

       % list_indexes contains indexes of publication in GS that matches pub in Scopus
        list_indexes = zeros(length(ith_author_scopus),1);
        % list_order contains the order of the author
        list_order = zeros(length(ith_author_scopus),1);
        list_totauthors = zeros(length(ith_author_scopus),1);
        list_isalphabetical = zeros(length(ith_author_scopus),1);
        list_isinetal = zeros(length(ith_author_scopus),1); % is the author in the et. al.
        list_isnotlisted = zeros(length(ith_author_scopus),1); % is the author is not even listed in the list

        for idx = 2:length(ith_author_scopus)
            
            for idy = 2:length(ith_author_gs)
                
                tmp_surnames = {};
                list_authors = split(ith_author_gs{idy}.authors,', ');
                for idz=1:length(list_authors)
                    surnames_list_authors = split(list_authors{idz});
                    surnames_list_authors = surnames_list_authors{end};
                    if ~strcmp(lower(surnames_list_authors),'...') 
                        tmp_surnames{end+1} = lower(surnames_list_authors);
                    end
                end
                if isequal(sort(tmp_surnames),tmp_surnames) % alphabetically ordered
                    is_alphabetically_ordered = 1;
                else 
                    is_alphabetically_ordered = 0;
                end
                

                if ~isempty(strfind(...
                    lower(ith_author_gs{idy}.title(regexp(lower(ith_author_gs{idy}.title),'[a-z]'))),...
                    lower(ith_author_scopus{idx}.title(regexp(lower(ith_author_scopus{idx}.title),'[a-z]')))...
                    ))

                    list_indexes(idx,1) = idy;
                    comma_pos = strfind(lower(ith_author_gs{idy}.authors),',');
                    tmporder = strfind(lower(ith_author_gs{idy}.authors),lower(surname));

                    if isempty(tmporder) % the author is listed within et.al. or ... or not in the list
                        if isempty(strfind(lower(ith_author_gs{idy}.authors),'...'))
                            list_order(idx,1) = 0;
                            list_totauthors(idx,1) = 0;
                            list_isinetal(idx,1) = 0;
                            list_isnotlisted(idx,1) = 1;
                        else
                            list_order(idx,1) = length(comma_pos);
                            list_totauthors(idx,1) = length(comma_pos);
                            list_isinetal(idx,1) = 1;
                            list_isnotlisted(idx,1) = 0;
                        end
                    else
                        list_isnotlisted(idx,1) = 0;
                        if isempty(comma_pos) % single author
                            list_order(idx,1) = 1;
                            list_totauthors(idx,1) = 1;
                        else
                            % tmporder should have the length of 1.
                            % But see http://sinta.ristekbrin.go.id/authors/detail?q=Real-Time+Electroencephalography-Based+Emotion+Recognition+System&search=1&id=29555&view=documentsgs
                            % the names in the list of author are duplicated

                            % proper search
                            list_authors = split(ith_author_gs{idy}.authors,', ');
                            
                            for idz=1:length(list_authors)
                                if ~isempty(strfind(lower(list_authors{idz}),lower(surname) ))
                                    if ~strcmp(surname,firstname)
                                        initial_name = lower(list_authors{idz});
                                        initial_name = initial_name(1);
                                        % this won't work for example HO Sinaga
                                        % sometimes the list says O Sinaga, sometimes it says HO Sinaga
                                        % if lower(firstname(1)) == initial_name
                                        %     order_author = idz;
                                        % end
                                        
                                        % revise
                                        % naively assign it
                                        order_author = idz;
                                    else
                                        order_author = idz;
                                    end
                                    break % use the first match only
                                end
                            end

                            list_order(idx,1) = order_author;
                            list_isalphabetical(idx,1) = is_alphabetically_ordered;

                            %% lazy search
                            % tmp = find(tmporder(1)>comma_pos); % in order to avoid the duplicated name

                            % if isempty(tmp) % first author
                            %     list_order(idx,1) = 1;
                            % else
                            %     list_order(idx,1) = max(tmp)+1;
                            % end

                            list_totauthors(idx,1) = length(comma_pos)+1;
                            assert(list_order(idx,1) <= list_totauthors(idx,1))
                        end
                    end
                end
            end
        end

        tmpstruct.list_indexes = list_indexes;
        tmpstruct.list_order = list_order;
        tmpstruct.list_totauthors = list_totauthors;
        tmpstruct.list_isalphabetical = list_isalphabetical;
        tmpstruct.list_isinetal = list_isinetal;
        cell_authorsip{end+1} = tmpstruct;

    end

    % delete(waitingBarDialog)
catch ME
    % delete(waitingBarDialog)
    rethrow(ME)
end

save(['cell_authorsip_',area,'.mat'],'cell_authorsip')