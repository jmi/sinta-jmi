% convert mat to json
% by: Ardimas Purwita (ardimasandipurwita@outlook.com)

%% Clear variables and close all figures
clear
close all

load cell_authorsip_art.mat
tmp = jsonencode(cell_authorsip);
fid = fopen('cell_authorship_art_v2.json','w');
fprintf(fid, '%s',tmp);
fclose(fid);

load cell_authorsip_health.mat
tmp = jsonencode(cell_authorsip);
fid = fopen('cell_authorship_health_v2.json','w');
fprintf(fid, '%s',tmp);
fclose(fid);

load cell_authorsip_science.mat
tmp = jsonencode(cell_authorsip);
fid = fopen('cell_authorship_science_v2.json','w');
fprintf(fid, '%s',tmp);
fclose(fid);

load cell_authorsip_social.mat
tmp = jsonencode(cell_authorsip);
fid = fopen('cell_authorship_social_v2.json','w');
fprintf(fid, '%s',tmp);
fclose(fid);

