%% Draw the performance for each author
% by: Ardimas Purwita (ardimasandipurwita@outlook.com)

%% Clear variables and close all figures
clear 
close all

load('P_art.mat')

field = fieldnames(P);

y = [];

for i=1:length(field)
    P_r = P.(field{i});
    tmp = struct2cell(P_r);
    y = [y;[tmp{:}]];
end

P_authors_art = sum(y,2);

load('P_social.mat')

field = fieldnames(P);

y = [];

for i=1:length(field)
    P_r = P.(field{i});
    tmp = struct2cell(P_r);
    y = [y;[tmp{:}]];
end

P_authors_social = sum(y,2);

load('P_health.mat')

field = fieldnames(P);

y = [];

for i=1:length(field)
    P_r = P.(field{i});
    tmp = struct2cell(P_r);
    y = [y;[tmp{:}]];
end

P_authors_health = sum(y,2);

load('P_science.mat')

field = fieldnames(P);

y = [];

for i=1:length(field)
    P_r = P.(field{i});
    tmp = struct2cell(P_r);
    y = [y;[tmp{:}]];
end

P_authors_science = sum(y,2);

figure
[cdf,x] = ecdf(log(P_authors_art));plot(x,cdf,'LineStyle','-')   
hold on                 
[cdf,x] = ecdf(log(P_authors_health));plot(x,cdf,'LineStyle','--')   
[cdf,x] = ecdf(log(P_authors_social));plot(x,cdf,'LineStyle',':')   
[cdf,x] = ecdf(log(P_authors_science));plot(x,cdf,'LineStyle','-.')  

legend({'art and humanities','health','social','science and engineering'},'Location','southeast') 

set(gca,'FontSize',16)
set(gca,'FontName','Times New Roman')
grid on
xlabel('$\log \sum_t P_{r,\mathcal{T}_t}$','interpreter','latex')
ylabel('CDF')