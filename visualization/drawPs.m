%% Draw the performance for each author
% by: Ardimas Purwita (ardimasandipurwita@outlook.com)

%% Clear variables and close all figures
clear 
close all

load('P_science.mat')

field = fieldnames(P);

y = [];

for i=1:length(field)
    P_r = P.(field{i});
    tmp = struct2cell(P_r);
    y = [y;[tmp{:}]];
end


for i = 1:6
    figure
    stem(y(:,i))
    grid on
    xlabel('Author $r$','interpreter','latex')
    % ylabel(['P\_L',num2str(i)])
    ylabel(['$P_{r,\mathcal{T}_',num2str(i),'}$'],'interpreter','latex')
    % xlabel(['$P_{\mathcal{T}_',num2str(i),'}$'],'interpreter','latex')
    set(gca,'FontSize',16)
    % set(gca,'FontSize',16)
    set(gca,'FontName','Times New Roman')
    % ylim([0,20])
    xlim([0,500])
    ylim([0,mean(y(:,i))])
    pbaspect([2,1,1])
    saveas(gcf,['./rawfigs_v2/PT', num2str(i) ,'_P_science.fig'])
    saveas(gcf,['./rawfigs_v2/PT', num2str(i) ,'_P_science.pdf'])
end
