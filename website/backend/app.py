import json
from datetime import datetime
import os

from flask import Flask, request, Response
from gevent.pywsgi import WSGIServer

# https://flask-restful.readthedocs.io/en/latest/quickstart.html
from flask_restful import Resource, Api

from pymongo import MongoClient

if "MONGODB_HOST" in os.environ:
    client = MongoClient("mongodb://%s:%s@mongodb:27017/?authSource=webapp"% (os.environ['MONGODB_USERNAME'],os.environ['MONGODB_PASSWORD']))
else:
    client = MongoClient("mongodb://%s:%s@localhost:27017/?authSource=webapp"% ('admin','password'))

db = client.webapp

app = Flask(__name__)

api = Api(app)


accessible_uri = [
    '/',
    '/api/webapp.json'];


class welcomeMessage(Resource):
    
    def get(self):
        
        now = datetime.now()
        dt_string = now.strftime("%d-%m-%Y_%H:%M:%S")
        
        # this reply should be accessible after a proper auth
        # this reply should also contain personal information, such as id 
        return {'welcome_message': 
            {'date_time':dt_string,
             'name': 'Guest',
             'accessible_uri_0': accessible_uri[0],
             'accessible_uri_1': accessible_uri[1]
            }
        }


api.add_resource(welcomeMessage, accessible_uri[0])

if __name__ == '__main__':
    # Using WSGI server to allow self contained server
    print("Listening on HTTP port 5000")
    http_server = WSGIServer(('', 5000), app)
    http_server.serve_forever()