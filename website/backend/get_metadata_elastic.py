from pymongo import MongoClient
import pprint
import json
import re
import os,sys
from math import nan
from tqdm import tqdm

import requests
from googlesearch import search
from bs4 import BeautifulSoup
from Levenshtein import distance as l_dist

import logging
from datetime import datetime
now = datetime.now()
dt_string = now.strftime("%d%m%Y_%H%M%S")

# check wheter the log folder exists
# if not create one
if not os.path.exists(os.getcwd() + '/log'):
    os.makedirs(os.getcwd() + '/log')

logging.basicConfig(filename='./log/get_metadata'+dt_string+'.log',level=logging.DEBUG)

# http://sinta.ristekbrin.go.id/authors/detail?page=2&id=3731&view=documentsscopus
__authors_pub_page_prefix__ = 'http://sinta.ristekbrin.go.id/authors/detail?page='
__authors_pub_page_infix__ = '&id='
__authors_pub_page_postfix__ = '&view=documentsscopus'

__min_idx__ = 0
__max_idx__ = 500

class ArgumentError(Exception):
    pass

def searchScimagojr(year,pubname,iswos = False):
    pubname = pubname.replace(':','')
    pubname = pubname.replace('(','')
    pubname = pubname.replace(')','')
    pubname = pubname.replace('"','')
    pubname = pubname.replace('"','')
    pubname = pubname.replace('/','')
    pubname = pubname.replace('-','')
    if not iswos:
        url = 'http://localhost:9200/scimagojr_all_'+str(year)+'/_search?'
    elif iswos:
        url = 'http://localhost:9200/scimagojr_wos_'+str(year)+'/_search?'
    data = {
        "query": {
            "query_string" : {
                "fields" : ["title"],
                "query" : pubname
            }
        }
    }
    header = {'Content-Type': 'application/json'};
    r = requests.get(url,json=data)
    tmp = r.content

    jtmp = json.loads(tmp.decode('utf8'))
    try:
        results =  jtmp['hits']['hits']
    except:
        print(jtmp)
        print(pubname)
        raise

    return results

def console():
    try:
        outputParsing = parse_args()
        outputParsing['minIdx']

        
        file = "./../../dataset/cell_authorship_v1.json";
        with open(file) as json_file:
            authorship = json.load(json_file)

        file = './../../dataset/sinta_scopus_v1.json';
        with open(file) as json_file:
            sinta_scopus = json.load(json_file)

        file = './../../dataset/sinta_pub_v1.json';
        with open(file) as json_file:
            sinta_gs = json.load(json_file)

        r = requests.get('http://localhost:9200')
        if r.status_code != 200:
            print("The elasticsearch server is not running yet!")
            raise

        metadata = {}

        # iterate over all 500 authors
        # for i in tqdm(range(0,len(sinta_scopus))):
        # for i in tqdm(range(0,1)):
        # for i in tqdm(range(0,3)):
        # for i in range(127,128):
        for i in tqdm(range(outputParsing['minIdx'],outputParsing['maxIdx'])):
        # for i in (range(outputParsing['minIdx'],outputParsing['maxIdx'])):
            logging.debug('i='+str(i))
            # get the i-th author id
            author_id = list(sinta_scopus.keys())[i];
            
            # get the i-th author scopus performance
            author_scopus = sinta_scopus[author_id]

            # get the i-th author name
            author_name = author_scopus[0]['name']
            
            metadata[author_id] = {'name':author_name}

            # remember that the values won't be exactly the same as the dataset were collected in the past
            metadata[author_id].update({'url':__authors_pub_page_prefix__+str(1)+__authors_pub_page_infix__+str(author_id)+__authors_pub_page_postfix__})

            # loop over all publications of the i-th author
            k = 1;
            for j in range(1,len(author_scopus)):
            # for j in range(12,13):
                
                # get the author list
                try:
                    author_list = sinta_gs[list(sinta_gs.keys())[i]][authorship[i]['list_indexes'][j]-1]['authors']
                except:
                    # http://sinta.ristekbrin.go.id/authors/detail?id=6669114&view=documentsgs
                    # the gs list is empty
                    author_list = ''
                # author_list = sinta_gs[list(sinta_gs.keys())[i]][authorship[i]['list_indexes'][j]-1]

                # get the title of publication
                title = author_scopus[j]['title']
                
                # get the publication name
                str_pub = author_scopus[j]['pub_name']
                tmp = str_pub.split(' | ') 
                pub_name = tmp[0]

                # get the total authors in the pub
                tot_author = authorship[i]['list_totauthors'][j]

                if tot_author != 0 and pub_name != '':

                    # get the pub year
                    pub_year = author_scopus[j]['pub_name'].split(' | ')[-2].split('-')[0]

                    # get the quartile
                    quartile = author_scopus[j]['quartile']

                    # get the number of citations
                    citation_num = author_scopus[j]['citation_num']

                    # get the total authors in the pub
                    tot_author = authorship[i]['list_totauthors'][j]

                    # get the order of the author in the pub
                    order_author = authorship[i]['list_order'][j]

                    # get the order of the author in the pub
                    is_alphabetical = authorship[i]['list_isalphabetical'][j]

                    # get the order of the author in the pub
                    is_in_et_al = authorship[i]['list_isinetal'][j]

                    # get the dicts of sjrs, hindex, and isWos per year
                    years = range(1999,2020)
                    # years = range(2018,2020)
                    scimagojr = {}
                    for year in years:
                        # find all instances from scimago_jr 
                        results = searchScimagojr(year,pub_name);

                        sjr,hindex,sjr_best_quartile,title_scimagojr_found = nan,nan,'-',''
                        for result in results:
                            sjr = result['_source']['sjr'].replace(',','.')
                            try:
                                sjr = float(sjr)
                            except:
                                sjr = nan
                            
                            hindex = result['_source']['h index']
                            sjr_best_quartile = result['_source']['sjr best quartile']

                            title_scimagojr_found = result['_source']['title']

                            if True:
                                if len(title_scimagojr_found) > len(pub_name):
                                    count = title_scimagojr_found.count(pub_name)
                                else:
                                    count = pub_name.count(title_scimagojr_found)

                                # if it's so different then ignore it
                                if count == 0:
                                    sjr,hindex,sjr_best_quartile = nan,nan,'-'

                            break

                            
                        # check if the pub name is in WOS list or not
                        results = searchScimagojr(year,pub_name,iswos = True)

                        isWos = False
                        if list(results):
                            isWos = True
                        
                        scimagojr[year]={}
                        scimagojr[year].update({ 
                                'sjr': str(sjr),
                                'hindex': str(hindex),
                                'iswos': isWos,
                                'sjr_best_quartile': sjr_best_quartile,
                                'title_scimagojr_found': title_scimagojr_found
                            })
                        

                    metadata[author_id][str(k-1)] = {}
                    metadata[author_id][str(k-1)].update({
                        'title': title,
                        'pub_year': pub_year,
                        'author_list': author_list,
                        'pub_name': pub_name,
                        'quartile': quartile,
                        'citation_num': citation_num,
                        'tot_author': tot_author,
                        'order_author': order_author,
                        'is_alphabetical': is_alphabetical,
                        'is_in_et_al': is_in_et_al,
                        'scimagojr':scimagojr
                        }
                        )

                    k = k+1



        # pprint.pprint(metadata)
        with open('metadata_'+dt_string+'.json','w') as json_file:
            json.dump(metadata,json_file)


    except(ArgumentError):
        sys.stderr.write(__usage__)

def parse_args():

    args = sys.argv[1:]

    minIdx = __min_idx__;
    maxIdx = __max_idx__;

    i = 0
    while i < len(args):
        op = args[i]
        if op in ("-h","--help"):
            raise ArgumentError
        if op in ("-v","--version"):
            print('Version: '+ str(__version__))
        elif op in ("--min-idx"):
            minIdx = int(args[i+1])
            i = i+1
        elif op in ("--max-idx"):
            maxIdx = int(args[i+1])
            i = i+1
        i = i+1 

    if (minIdx < __min_idx__) | (maxIdx > __max_idx__):
        logging.error('page number must be between ' + str(__min_idx__) + ' and ' + str(__max_idx__))
        raise ArgumentError

    logging.info('index number:' + str(minIdx) + '-' + str(maxIdx))

    return {'minIdx':minIdx, # -1 due to the indexing starts from 0
            'maxIdx':maxIdx
            }

if __name__ == '__main__':

    try:
        
        console()

    except (KeyboardInterrupt, SystemExit):
        pass

