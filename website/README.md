# README

----
## backend and scimagojr_csv

The main pupose of this is that to do a quick search of SJR by using mongodb. 

Given that docker and docker-compose are already installed, the following are the steps to run the mongodb server

1. Run the service

```
sudo docker-compose up --build
```

2. Copy the SJR dataset to the mongodb container

```
sudo docker cp ./scimagojr_csv/ mongo:/home
```

3. Run the mongodb container bash

```
docker exec -it mongo bash
```

4. Import the csv files

```
for i in {1999..2019}; do mongoimport --db webapp --collection scimagojr_wos_$i --authenticationDatabase admin --username admin --password password --type csv --headerline --drop --file scimagojr_wos_$i.csv; done

for i in {1999..2019}; do mongoimport --db webapp --collection scimagojr_all_$i --authenticationDatabase admin --username admin --password password --type csv --headerline --drop --file scimagojr_all_$i.csv; done
```




----
## Contributors

* **Ardimas Purwita** - *Initial work* - ardimasandipurwita@outlook.com
