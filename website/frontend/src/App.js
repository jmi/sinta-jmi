import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MaterialTable from 'material-table';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { Bar } from 'react-chartjs-2';
import palette from './palette';
import { data, options } from './chart';

const useStyles = makeStyles((theme) => ({
  table: {
    // minWidth: 650,
    borderStyle: '1px solid rgba(224, 224, 224, 1)'
  },
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));

var Latex = require('react-latex');


function createData(name, score) {
  return { name, score};
}

function randomVal(){
  return Math.floor(Math.random() * 90 + 10);
}

const dataAuth = [randomVal(),randomVal(),randomVal()]

const rows = [
  createData(<Latex>$P_1$</Latex>, dataAuth[0]),
  createData(<Latex>$P_2$</Latex>, dataAuth[1]),
  createData(<Latex>$P_3$</Latex>, dataAuth[2]),  
];

const dataChart = {
  labels: ['P1', 'P2', 'P3'],
  datasets: [
    {
      label: 'Performance',
      backgroundColor: palette.primary.main,
      data: [dataAuth[0],dataAuth[1],dataAuth[2]]
    }
  ]
};


export default function App() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={3} justify='center'>

        <Grid item md={10} justify='center'>
          <Typography variant="h4" style={{'textAlign':'center'}}>
            Utility Maximization for New Ranking Criterion of the SINTA Indonesia Researchers Indexing System
          </Typography>
        </Grid>
        <Grid item md={8} justify='center'>
          <Typography variant="h6" style={{'textAlign':'center'}}>
            Khoirul&nbsp;Anwar<sup>*</sup>, Agus&nbsp;Kusnayat, Erwin&nbsp;Budi&nbsp;Setiawan, Muhammad&nbsp;Reza&nbsp;Kahar&nbsp;Aziz, Ardimas&nbsp;Andi&nbsp;Purwita, Ade&nbsp;Irawan, Erna&nbsp;Febriyanti, and Catur&nbsp;Prasetiawan
            {/*Khoirul Anwar<sup>a,*</sup>, Agus Kusnayat<sup>b</sup>, Erwin Budi Setiawan<sup>c</sup>, Muhammad Reza Kahar Aziz<sup>c</sup>, Ardimas Andi Purwita<sup>e</sup>, Ade Irawan<sup>f</sup>, Erna Febriyanti<sup>b</sup>, and Catur Prasetiawan<sup>g</sup>*/}
          </Typography>
        </Grid>
        <Grid item md={10} justify='center'>
          <MaterialTable
            title="Table: Proposed Scoring System"
            columns={[
              { title: 'ID', field: 'id' ,
              render: rowData => <Link href={'http://sinta.ristekbrin.go.id/authors/detail?page=1&id='+rowData.id} target="_blank"> {rowData.id} </Link>
              },
              { title: '3 Year Score', field: 'threeyearscorev1' },
              { title: 'All Year Score', field: 'allyearscorev1' },
              { title: '3 Year Score v2', field: 'threeyearscorev2' },
              { title: 'All Year Score v2', field: 'allyearscorev2' },
              { title: 'Proposed Score', field: 'proposedscore',
              headerStyle: {
                  fontWeight: 'bold',
                },
                cellStyle: {
                  fontWeight: 'bold',
                } },
            ]}
            data={[
              { id: 3731   , threeyearscorev1: randomVal(), allyearscorev1: randomVal(), threeyearscorev2: randomVal(), allyearscorev2: randomVal(), proposedscore: randomVal(), p1: randomVal()},
              { id: 59706  , threeyearscorev1: randomVal(), allyearscorev1: randomVal(), threeyearscorev2: randomVal(), allyearscorev2: randomVal(), proposedscore: randomVal(), p1: randomVal()},
              { id: 6005991, threeyearscorev1: randomVal(), allyearscorev1: randomVal(), threeyearscorev2: randomVal(), allyearscorev2: randomVal(), proposedscore: randomVal(), p1: randomVal()},
              { id: 29555  , threeyearscorev1: randomVal(), allyearscorev1: randomVal(), threeyearscorev2: randomVal(), allyearscorev2: randomVal(), proposedscore: randomVal(), p1: randomVal()},
              { id: 5982442, threeyearscorev1: randomVal(), allyearscorev1: randomVal(), threeyearscorev2: randomVal(), allyearscorev2: randomVal(), proposedscore: randomVal(), p1: randomVal()},
              { id: 6005015, threeyearscorev1: randomVal(), allyearscorev1: randomVal(), threeyearscorev2: randomVal(), allyearscorev2: randomVal(), proposedscore: randomVal(), p1: randomVal()},
              { id: 260404 , threeyearscorev1: randomVal(), allyearscorev1: randomVal(), threeyearscorev2: randomVal(), allyearscorev2: randomVal(), proposedscore: randomVal(), p1: randomVal()},
              { id: 5977796, threeyearscorev1: randomVal(), allyearscorev1: randomVal(), threeyearscorev2: randomVal(), allyearscorev2: randomVal(), proposedscore: randomVal(), p1: randomVal()},
              { id: 259819 , threeyearscorev1: randomVal(), allyearscorev1: randomVal(), threeyearscorev2: randomVal(), allyearscorev2: randomVal(), proposedscore: randomVal(), p1: randomVal()},
              { id: 5974510, threeyearscorev1: randomVal(), allyearscorev1: randomVal(), threeyearscorev2: randomVal(), allyearscorev2: randomVal(), proposedscore: randomVal(), p1: randomVal()},
            ]}
            detailPanel={[
              {
                tooltip: 'Show Name',
                render: rowData => {
                  return (
                    // foo(rowData.p1)
                    <Grid container spacing={3} >
                    <Grid item md={3} xs={5}>
                    <TableContainer component={Paper}>
                      <Table className={classes.table} size="small" aria-label="a dense table">
                        <TableHead>
                          <TableRow>
                            <TableCell>Performance</TableCell>
                            <TableCell align="right">Value</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {rows.map((row) => (
                            <TableRow key={row.name}>
                              <TableCell component="th" scope="row">
                                {row.name}
                              </TableCell>
                              <TableCell align="right">{row.score}</TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                    </Grid>
                    <Grid item md={9} xs={7}>
                      <Bar
                        data={dataChart}
                        options={options}
                      />
                    </Grid>
                    </Grid>
                  )
                },
              }
            ]}
          />
        </Grid>
      </Grid>
    </div>
  )
}

                      // {tryBokeh()}
